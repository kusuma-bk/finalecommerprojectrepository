package com.org.e_commerce.utility;

public class ProductsUtility {
	private ProductsUtility() {
	    throw new IllegalStateException("ProductsUtility class");
	  }
	
	public static final String PRODUCTS_LIST_ERROR = "There are no products";
	public static final int PRODUCTS_LIST_ERROR_STATUS = 606;

}
