package com.org.e_commerce.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.org.e_commerce.utility.CustomerUtility;
import com.org.e_commerce.utility.ProductsUtility;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
	@ExceptionHandler(CustomerException.class)

	public ResponseEntity<ErrorResponse> customerErrorException(CustomerException ex) {
		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(CustomerUtility.CUSTOMER_EXISTS_ERROR_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(ProductException.class)
	public ResponseEntity<ErrorResponse> productsErrorException(ProductException ex) {
		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(ProductsUtility.PRODUCTS_LIST_ERROR_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(UserExistsErrorException.class)
	public ResponseEntity<ErrorResponse> userExistsErrorException(UserExistsErrorException ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(CustomerUtility.USER_ERROR_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);

	}

	@ExceptionHandler(UserPasswordErrorException.class)
	public ResponseEntity<ErrorResponse> userPasswordErrorException(UserPasswordErrorException ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(CustomerUtility.USER_PSWD_ERROR_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);

	}

	@ExceptionHandler(UserAlreadyExists.class)
	public ResponseEntity<ErrorResponse> userAreadyExistsException(UserAlreadyExists ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(CustomerUtility.USERALREADY_EXISTS_STATUSCODE);

		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);

	}
}
