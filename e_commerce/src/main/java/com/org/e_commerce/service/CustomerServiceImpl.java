package com.org.e_commerce.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.e_commerce.dto.CustomerDto;
import com.org.e_commerce.dto.CustomerLoginResponseDto;
import com.org.e_commerce.dto.LoginRequestDto;
import com.org.e_commerce.dto.ProductListResponseDto;
import com.org.e_commerce.entity.Customer;
import com.org.e_commerce.entity.ProductUser;
import com.org.e_commerce.exception.CustomerException;
import com.org.e_commerce.exception.ProductException;
import com.org.e_commerce.exception.UserAlreadyExists;
import com.org.e_commerce.exception.UserExistsErrorException;
import com.org.e_commerce.exception.UserPasswordErrorException;
import com.org.e_commerce.repository.CustomerRepository;
import com.org.e_commerce.repository.ProductUserRepository;
import com.org.e_commerce.repository.ProductsRepository;
import com.org.e_commerce.utility.CustomerUtility;
import com.org.e_commerce.utility.ProductsUtility;

@Service
public class CustomerServiceImpl implements CustomerService {

	private static final Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);

	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	ProductsRepository productsRepository;

	@Autowired
	ProductUserRepository productUserRepository;

	public String customerRegistartion(CustomerDto customerDto) throws UserAlreadyExists {
		Customer customer ;
		logger.info("inside customerRegistartion method of CustomerServiceImpl class");

		Optional<Customer> userDetail = customerRepository.findByEmailId(customerDto.getEmailId());

		if (userDetail.isPresent()) {
			throw new UserAlreadyExists(CustomerUtility.USERALREADY_EXISTS);
		}
		customer= new Customer();
		BeanUtils.copyProperties(customerDto, customer);
		customerRepository.save(customer);
		return "CustomerDetails registered Successfully";
	}

	@Override
	public List<ProductListResponseDto> productsList(long customerId) throws CustomerException, ProductException {
		logger.info("inside productsList method of CustomerServiceImpl class");
		// List<ProductUser> listOfProductUser = new ArrayList<>();
		List<ProductUser> listOfProductUser;
		ProductListResponseDto productUserResponse;
		List<ProductListResponseDto> listproductUserResponse = new ArrayList<>();

		Optional<Customer> optionalCustomer = customerRepository.findById(customerId);
		if (!optionalCustomer.isPresent()) {
			throw new CustomerException(CustomerUtility.CUSTOMER_EXISTS_ERROR);
		}

		if (optionalCustomer.get().getCustomerType().equalsIgnoreCase("priority")) {
			listOfProductUser = productUserRepository.findByCustomerType("priority");
			if (listOfProductUser.isEmpty()) {
				throw new ProductException(ProductsUtility.PRODUCTS_LIST_ERROR);
			}
			for (ProductUser productUser : listOfProductUser) {
				productUserResponse = new ProductListResponseDto();
				BeanUtils.copyProperties(productUser, productUserResponse);
				listproductUserResponse.add(productUserResponse);
			}
		} else if (optionalCustomer.get().getCustomerType().equalsIgnoreCase("normal")) {
			listOfProductUser = productUserRepository.findByCustomerType("normal");
			if (listOfProductUser.isEmpty()) {
				throw new ProductException(ProductsUtility.PRODUCTS_LIST_ERROR);
			}
			for (ProductUser productUser : listOfProductUser) {
				productUserResponse = new ProductListResponseDto();
				BeanUtils.copyProperties(productUser, productUserResponse);
				listproductUserResponse.add(productUserResponse);
			}

		}

		return listproductUserResponse;

	}

	@Override
	public CustomerLoginResponseDto customerLogin(LoginRequestDto loginRequestDto)
			throws UserExistsErrorException, UserPasswordErrorException {
		logger.info("inside customerLogin method of CustomerServiceImpl class");
		Optional<Customer> userDetail = customerRepository.findByEmailIdAndPassword(loginRequestDto.getEmailId(),
				loginRequestDto.getPassword());
		if (!userDetail.isPresent()) {

			Optional<Customer> userDetail1 = customerRepository.findByEmailId(loginRequestDto.getEmailId());
			if (!userDetail1.isPresent())

				throw new UserExistsErrorException(CustomerUtility.USER_ERROR);

			throw new UserPasswordErrorException(CustomerUtility.USER_PSWD_ERROR);
		}

		CustomerLoginResponseDto customerLoginResponseDto = new CustomerLoginResponseDto();
		BeanUtils.copyProperties(userDetail.get(), customerLoginResponseDto);
		customerLoginResponseDto.setStatuscode(611);
		return customerLoginResponseDto;
	}

}
