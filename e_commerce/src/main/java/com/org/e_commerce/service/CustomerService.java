package com.org.e_commerce.service;

import java.util.List;

import com.org.e_commerce.dto.CustomerDto;
import com.org.e_commerce.dto.CustomerLoginResponseDto;
import com.org.e_commerce.dto.LoginRequestDto;
import com.org.e_commerce.dto.ProductListResponseDto;
import com.org.e_commerce.exception.CustomerException;
import com.org.e_commerce.exception.ProductException;
import com.org.e_commerce.exception.UserAlreadyExists;
import com.org.e_commerce.exception.UserExistsErrorException;
import com.org.e_commerce.exception.UserPasswordErrorException;

public interface CustomerService {
	List<ProductListResponseDto> productsList(long customerId) throws CustomerException, ProductException;

	public String customerRegistartion(CustomerDto customerDto) throws UserAlreadyExists;

	CustomerLoginResponseDto customerLogin(LoginRequestDto loginRequestDto)
			throws UserExistsErrorException, UserPasswordErrorException;
}
