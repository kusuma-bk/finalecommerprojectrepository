package com.org.e_commerce.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
public class Products {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)

	private long productId;
	private String productName;
	private double productPrice;
	private String description;

}
