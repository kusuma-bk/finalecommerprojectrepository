package com.org.e_commerce.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
public class ProductUser {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String customerType;
	private long productId;
	private String productName;
	private String description;
	private double price;

}
