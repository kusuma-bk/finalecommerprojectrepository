package com.org.e_commerce.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.org.e_commerce.dto.ProductDto;
import com.org.e_commerce.dto.ProductListResponseDto;
import com.org.e_commerce.dto.ProductResponseDto;
import com.org.e_commerce.dto.ProductsListResponseDto;
import com.org.e_commerce.entity.Product;
import com.org.e_commerce.entity.ProductDetails;
import com.org.e_commerce.exception.ProductDetailsException;
import com.org.e_commerce.exception.ProductException;
import com.org.e_commerce.repository.ProductDetailsRepository;
import com.org.e_commerce.repository.ProductRepository;
import com.org.e_commerce.utility.ProductsUtility;

@Service
public class ProductServiceImpl implements ProductService {
	private static final Logger logger = LoggerFactory.getLogger(ProductServiceImpl.class);
	@Autowired
	ProductRepository productRepository;

	@Autowired
	ProductDetailsRepository productDetailsRepository;

	@Override
	public ProductsListResponseDto searchProduct(String productName) throws ProductException {
		List<ProductListResponseDto> listOfProductResponseDto = new ArrayList<>();
		ProductListResponseDto productListResponseDto;
		ProductsListResponseDto productsListResponseDto = new ProductsListResponseDto();
		List<Product> listOfProducts = productRepository.findByProductNameIgnoreCaseContaining(productName);
		if (listOfProducts.isEmpty()) {
			throw new ProductException(ProductsUtility.PRODUCTS_LIST_ERROR);
		}
		for (Product product : listOfProducts) {

			Optional<ProductDetails> productDetails = productDetailsRepository.findByProductId(product.getProductId());

			if (!productDetails.isPresent()) {
				throw new ProductException(ProductsUtility.PRODUCTS_LIST_ERROR);
			}

			productListResponseDto = new ProductListResponseDto();
			BeanUtils.copyProperties(product, productListResponseDto);
			BeanUtils.copyProperties(productDetails.get(), productListResponseDto);
			listOfProductResponseDto.add(productListResponseDto);

			productsListResponseDto.setProductListResponseDto(listOfProductResponseDto);
			productsListResponseDto.setStatusCode(ProductsUtility.PRODUCT_LIST_STATUS);
		}

		return productsListResponseDto;
	}

	@Override
	public ProductResponseDto addProducts(ProductDto productDto) throws ProductDetailsException {
		ProductDetails productDetails = null;
		logger.info("inside addProducts method of ProductServiceImpl class");
		String productName = productDto.getProductName();
		Double price = productDto.getProductPrice();
		String description = productDto.getDescription();
		int quantity = productDto.getQuantity();
		logger.info("inside addProducts method of ProductServiceImpl class");
		ProductResponseDto productResponseDto = new ProductResponseDto();
		if (ObjectUtils.isEmpty(productDto)) {
			throw new ProductDetailsException(ProductsUtility.TRY_AGAIN);
		}
		Product product = new Product();
		BeanUtils.copyProperties(productDto, product);
		Product productIn = productRepository.findByProductNameIgnoreCase(productName);
		ProductDetails productDetailsIn = null;
		long productDetailsId = 0;
		if (ObjectUtils.isEmpty(productIn)) {

			Product product1 = productRepository.save(product);
			productResponseDto.setMessage(ProductsUtility.PRODUCT_ADDED_SUCCESS);
			productResponseDto.setStatusCode(ProductsUtility.PRODUCT_ADDED_SUCCESS_STATUS);
			productDetails = new ProductDetails();
			productDetails.setProductId(product1.getProductId());
			productDetails.setQuantity(product1.getQuantity());
			productDetailsIn = productDetailsRepository.save(productDetails);
			productDetailsId = productDetailsIn.getProductDetailsId();
			return productResponseDto;
		} else {
			int quantityIn = productIn.getQuantity();
			productIn.setProductPrice(price);
			productIn.setDescription(description);
			productIn.setQuantity(quantityIn + quantity);
			productRepository.save(productIn);
			Optional<ProductDetails> productDetailsOpt = productDetailsRepository
					.findByProductId(productIn.getProductId());
			int productDetailsQuantity = productDetailsOpt.get().getQuantity();
			productDetailsOpt.get().setQuantity(productDetailsQuantity + productDto.getQuantity());
			productDetailsRepository.save(productDetailsOpt.get());
			productResponseDto.setMessage(ProductsUtility.PRODUCT_UPDATED_SUCCESS);
			productResponseDto.setStatusCode(ProductsUtility.PRODUCT_ADDED_SUCCESS_STATUS);
			return productResponseDto;
		}
	}
}
