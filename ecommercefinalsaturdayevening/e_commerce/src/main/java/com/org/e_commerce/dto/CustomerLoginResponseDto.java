package com.org.e_commerce.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CustomerLoginResponseDto {
	private long customerId;
	private String customerName;
	private String emailId;
	private int statuscode;
	private String customerType;
	private int loginId;

}
