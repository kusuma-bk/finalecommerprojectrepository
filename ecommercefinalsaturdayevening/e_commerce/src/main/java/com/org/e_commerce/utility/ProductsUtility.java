package com.org.e_commerce.utility;

public class ProductsUtility {
	private ProductsUtility() {
		throw new IllegalStateException("ProductsUtility class");
	}

	public static final String PRODUCTS_LIST_ERROR = "There are no products";
	public static final int PRODUCTS_LIST_ERROR_STATUS = 606;

	public static final String TRY_AGAIN = "Unable to save the details something went wrong!!";
	public static final int TRY_AGAIN_STATUSCODE = 660;

	public static final String PRODUCT_ADDED_SUCCESS = "Products added successfully";
	public static final int PRODUCT_ADDED_SUCCESS_STATUS = 701;
	
	public static final String PRODUCT_UPDATED_SUCCESS = "Products updated successfully";
	public static final int PRODUCT_UPDATED_SUCCESS_STATUS = 701;

	public static final int PRODUCT_LIST_STATUS = 700;

}
