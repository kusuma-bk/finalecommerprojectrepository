package com.org.e_commerce.customerservicetest;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.org.e_commerce.dto.PurchaseDto;
import com.org.e_commerce.dto.PurchaseListResponseDto;
import com.org.e_commerce.dto.PurchaseResponseDto;
import com.org.e_commerce.dto.RatingRequestDto;
import com.org.e_commerce.dto.RatingResponseDto;
import com.org.e_commerce.entity.Product;
import com.org.e_commerce.entity.ProductDetails;
import com.org.e_commerce.entity.ProductUser;
import com.org.e_commerce.entity.Purchase;
import com.org.e_commerce.exception.QuantityException;
import com.org.e_commerce.repository.ProductDetailsRepository;
import com.org.e_commerce.repository.ProductRepository;
import com.org.e_commerce.repository.ProductUserRepository;
import com.org.e_commerce.repository.PurchaseRepository;
import com.org.e_commerce.service.PurchaseServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class PurchaseServiceImplTest {

	@Mock
	PurchaseRepository purchaseRepository;

	@Mock
	ProductDetailsRepository productDetailsRepository;

	@Mock
	ProductRepository productRepository;

	@Mock
	ProductUserRepository productUserRepository;

	@InjectMocks
	PurchaseServiceImpl purchaseServiceImpl;

	PurchaseResponseDto purchaseResponseDto;
	PurchaseDto purchaseDto;
	PurchaseDto purchaseDto1;
	ProductDetails productDetails;
	List<ProductUser> listOfProductUserPException;
	Purchase purchase;
	RatingRequestDto ratingRequestDto;
	List<Purchase> purchaseList;
	long customerId;
	Product products;

	@Before
	public void setup() {

		purchaseDto = new PurchaseDto();
		purchaseDto.setCustomerId(2);
		purchaseDto.setProductId(1);
		purchaseDto.setQuantity(6);

		purchaseDto1 = new PurchaseDto();
		purchaseDto1.setCustomerId(2);
		purchaseDto1.setProductId(1);
		purchaseDto1.setQuantity(3);

		purchaseResponseDto = new PurchaseResponseDto();
		purchaseResponseDto.setMessage("Successfully you have purchased the Product");
		purchaseResponseDto.setStatus(621);

		productDetails = new ProductDetails();
		productDetails.setProductDetailsId(1);
		productDetails.setProductId(2);
		productDetails.setProductRating(3);
		productDetails.setQuantity(5);

		purchase = new Purchase();
		purchase.setCustomerId(2);
		purchase.setProductId(1);
		purchase.setQuantity(3);
		purchase.setPurchasedDate(LocalDateTime.now());

		ratingRequestDto = new RatingRequestDto();
		ratingRequestDto.setPurchaseId(1);
		ratingRequestDto.setCustomerRating(5);

		purchaseList = new ArrayList<>();
		purchaseList.add(purchase);

		products = new Product();

		products.setDescription("mobile");
		products.setProductId(1);
		products.setProductName("samsung");
		products.setProductPrice(30000);
		products.setQuantity(5);

	}

	@Test
	public void buyProductTest() throws QuantityException {
		Mockito.when(productDetailsRepository.findByProductId(Mockito.anyLong()))
				.thenReturn(Optional.of(productDetails));
		Mockito.when(purchaseRepository.save(Mockito.any(Purchase.class))).thenReturn(purchase);
		PurchaseResponseDto response = purchaseServiceImpl.buyProduct(purchaseDto1);
		Assert.assertEquals(purchaseResponseDto.getMessage(), response.getMessage());
	}

	@Test(expected = QuantityException.class)
	public void buyProductQuantityError() throws QuantityException {

		Mockito.when(productDetailsRepository.findByProductId(Mockito.anyLong()))
				.thenReturn(Optional.of(productDetails));

		purchaseServiceImpl.buyProduct(purchaseDto);

	}

	@Test
	public void ratingTest() {
		Mockito.when(purchaseRepository.findByPurchaseId(Mockito.anyLong())).thenReturn(Optional.of(purchase));
		Mockito.when(purchaseRepository.save(purchase)).thenReturn(purchase);
		Mockito.when(productDetailsRepository.findByProductId(Mockito.anyLong()))
				.thenReturn(Optional.of(productDetails));
		Mockito.when(productDetailsRepository.save(productDetails)).thenReturn(productDetails);

		RatingResponseDto response = purchaseServiceImpl.rating(ratingRequestDto);

		Assert.assertNotNull(response);

		Assert.assertEquals("Thanks for your feedback", response.getMessage());
	}

	
	  @Test public void purchaseListTest() {
	  Mockito.when(purchaseRepository.findByCustomerId(Mockito.anyLong())).
	  thenReturn(purchaseList);
	  Mockito.when(productRepository.findByProductId(Mockito.anyLong())).thenReturn
	  (Optional.of(products)); List<PurchaseListResponseDto> response =
	  purchaseServiceImpl.purchaseList(1); Assert.assertNotNull(response);
	  Assert.assertEquals(products.getProductName(),
	  response.get(0).getProductName());
	  
	  }
	 

}