package com.org.e_commerce.customercontrollertest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.org.e_commerce.controller.ProductController;
import com.org.e_commerce.dto.ProductDto;
import com.org.e_commerce.dto.ProductListResponseDto;
import com.org.e_commerce.dto.ProductResponseDto;
import com.org.e_commerce.dto.ProductsListResponseDto;
import com.org.e_commerce.exception.ProductDetailsException;
import com.org.e_commerce.exception.ProductException;
import com.org.e_commerce.service.ProductService;

@RunWith(MockitoJUnitRunner.class)
public class ProductControllerTest {

	@Mock
	ProductService productService;



	@InjectMocks
	ProductController productController;

	ProductsListResponseDto productsListResponseDto;
	String productName;

	List<ProductListResponseDto> productListResponseDto;
	ProductResponseDto productResponseDto;
	ProductDto productDto;
	long customerId;

	@Before
	public void setup() {
		productsListResponseDto = new ProductsListResponseDto();
		productName = "HP Laptop";
		customerId = 1l;

		productListResponseDto = new ArrayList<ProductListResponseDto>();

		productResponseDto = new ProductResponseDto();
		productResponseDto.setMessage("Products Added Successfully");
		productResponseDto.setStatusCode(701);

		productDto = new ProductDto();
		productDto.setProductName("refrigerator");
		productDto.setProductPrice(10000);
		productDto.setDescription("homeAppliance");
		productDto.setQuantity(7);
	}

	@Test
	public void searchProduct() throws ProductException {

		Mockito.when(productService.searchProduct(Mockito.anyString())).thenReturn(productsListResponseDto);
		ResponseEntity<ProductsListResponseDto> actualValue = productController.searchProduct(productName);
		Assert.assertEquals(productsListResponseDto, actualValue.getBody());

	}

	@Test
	public void addProducts() throws ProductDetailsException {

		Mockito.when(productService.addProducts(Mockito.any(ProductDto.class))).thenReturn(productResponseDto);

		ResponseEntity<ProductResponseDto> response = productController.addProducts(productDto);
		Assert.assertEquals(productResponseDto, response.getBody());

	}


}
