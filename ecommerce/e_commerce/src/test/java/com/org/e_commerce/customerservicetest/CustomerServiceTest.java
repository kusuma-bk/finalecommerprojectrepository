package com.org.e_commerce.customerservicetest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.org.e_commerce.dto.CustomerDto;
import com.org.e_commerce.dto.CustomerLoginResponseDto;
import com.org.e_commerce.dto.LoginRequestDto;
import com.org.e_commerce.dto.ProductListResponseDto;
import com.org.e_commerce.entity.Customer;
import com.org.e_commerce.entity.Product;
import com.org.e_commerce.entity.ProductUser;
import com.org.e_commerce.exception.UserAlreadyExists;
import com.org.e_commerce.exception.UserExistsErrorException;
import com.org.e_commerce.exception.UserPasswordErrorException;
import com.org.e_commerce.repository.CustomerRepository;
import com.org.e_commerce.repository.ProductRepository;
import com.org.e_commerce.repository.ProductUserRepository;
import com.org.e_commerce.service.CustomerServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceTest {

	@Mock
	CustomerRepository customerRepository;

	@Mock
	ProductUserRepository productUserRepository;

	@Mock
	ProductRepository productRepository;

	@InjectMocks
	CustomerServiceImpl customerServiceImpl;

	CustomerLoginResponseDto customerLoginResponseDto;
	LoginRequestDto loginRequestDto;
	Customer customer;
	CustomerDto customerDto;
	List<ProductUser> listOfProductUserP;
	List<ProductUser> listOfProductUserN;
	ProductListResponseDto productListResponseDto;
	ProductUser productUser;
	ProductUser productUserN;

	List<ProductUser> listOfProductUserPException;
	List<Product> listOfProduct;
	Product product;
	Product product1;
	long customerId;

	@Before
	public void setup() {
		customerLoginResponseDto = new CustomerLoginResponseDto();
		customerLoginResponseDto.setCustomerId(1);
		customerLoginResponseDto.setCustomerName("suppi");
		customerLoginResponseDto.setEmailId("suppi@gmail.com");

		loginRequestDto = new LoginRequestDto();
		loginRequestDto.setEmailId("suppi@gmail.com");
		loginRequestDto.setPassword("123");

		customer = new Customer();
		customer.setCustomerId(1);
		customer.setCustomerName("suppi");
		customer.setCustomerType("normal");
		customer.setEmailId("suppi@gmail.com");
		customer.setPassword("123");
		customer.setPhoneNumber("9591881761");

		customerDto = new CustomerDto();
		customerDto.setCustomerName("kusuma");
		customerDto.setCustomerType("priority");
		customerDto.setEmailId("kusuma@gmail.com");
		customerDto.setPassword("456");
		customerDto.setPhoneNumber("8765788590");

		customerId = 1l;

		listOfProductUserP = new ArrayList<>();
		listOfProductUserN = new ArrayList<>();

		productListResponseDto = new ProductListResponseDto();
		productListResponseDto.setDescription("electronic");
		productListResponseDto.setProductPrice(20000);
		productListResponseDto.setProductId(1l);
		productListResponseDto.setProductName("iPhone");
		productUser = new ProductUser();
		productUser.setCustomerType("priority");
		productUser.setDescription("electronic");
		productUser.setId(1l);
		productUser.setPrice(20000);
		productUser.setProductId(1l);
		productUser.setProductName("iPhone");
		listOfProductUserP.add(productUser);

		productUserN = new ProductUser();
		productUserN.setCustomerType("normal");
		productUserN.setDescription("electronic");
		productUserN.setId(1l);
		productUserN.setPrice(20000);
		productUserN.setProductId(1l);
		productUserN.setProductName("iPhone");
		listOfProductUserN.add(productUserN);
		listOfProductUserPException = new ArrayList<>();
		product = new Product();
		product1 = new Product();
		listOfProduct = new ArrayList<>(20);
		product.setDescription("electronic");
		product.setProductId(1l);
		product.setProductName("iphone");
		product.setProductPrice(10000);
		product.setQuantity(5);

		product1.setDescription("electronic");
		product1.setProductId(2l);
		product1.setProductName("mac");
		product1.setProductPrice(10000);
		product1.setQuantity(5);
		listOfProduct.add(product);
		listOfProduct.add(product1);

	}

	@Test
	public void customerLogin() throws UserExistsErrorException, UserPasswordErrorException {

		Mockito.when(customerRepository.findByEmailIdAndPassword(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(Optional.of(customer));
		CustomerLoginResponseDto response = customerServiceImpl.customerLogin(loginRequestDto);
		Assert.assertNotNull(response);
		Assert.assertEquals(1l, response.getCustomerId());
	}

	@Test(expected = UserExistsErrorException.class)
	public void customerLoginEmailIdError() throws UserExistsErrorException, UserPasswordErrorException {

		Mockito.when(customerRepository.findByEmailIdAndPassword(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(Optional.empty());
		Mockito.when(customerRepository.findByEmailId(Mockito.anyString())).thenReturn(Optional.empty());
		customerServiceImpl.customerLogin(loginRequestDto);

	}

	@Test(expected = UserPasswordErrorException.class)
	public void customerLoginPasswordError() throws UserExistsErrorException, UserPasswordErrorException {
		Mockito.when(customerRepository.findByEmailId(Mockito.anyString())).thenReturn(Optional.of(customer));
		Mockito.when(customerRepository.findByEmailIdAndPassword(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(Optional.empty());

		customerServiceImpl.customerLogin(loginRequestDto);

	}

	@Test
	public void customerRegistration() throws UserAlreadyExists {
		Mockito.when(customerRepository.findByEmailId(Mockito.anyString())).thenReturn(Optional.empty());
		Mockito.when(customerRepository.save(Mockito.any(Customer.class))).thenReturn(customer);
		String response = customerServiceImpl.customerRegistartion(customerDto);
		Assert.assertEquals("CustomerDetails registered Successfully", response);
	}

	@Test(expected = UserAlreadyExists.class)
	public void customerError() throws UserAlreadyExists {
		Mockito.when(customerRepository.findByEmailId(Mockito.anyString())).thenReturn(Optional.of(customer));
		customerServiceImpl.customerRegistartion(customerDto);

	}

	/*
	 * @Test public void productsListTest() throws CustomerException,
	 * ProductException {
	 * Mockito.when(customerRepository.findById(Mockito.anyLong())).thenReturn(
	 * Optional.of(customer));
	 * Mockito.when(productUserRepository.findByCustomerType(Mockito.anyString())).
	 * thenReturn(listOfProductUserP);
	 * 
	 * List<ProductListResponseDto> actual =
	 * customerServiceImpl.productsList(customerId);
	 * 
	 * Assert.assertNotNull(actual); Assert.assertEquals(1, actual.size()); }
	 * 
	 * @Test(expected = CustomerException.class) public void
	 * productsListTestCustomerException() throws CustomerException,
	 * ProductException {
	 * Mockito.when(customerRepository.findById(Mockito.anyLong())).thenReturn(
	 * Optional.empty()); customerServiceImpl.productsList(customerId); }
	 * 
	 * @Test(expected = ProductException.class) public void
	 * productsListTestProductPException() throws CustomerException,
	 * ProductException {
	 * Mockito.when(customerRepository.findById(Mockito.anyLong())).thenReturn(
	 * Optional.of(customer));
	 * 
	 * Mockito.when(productUserRepository.findByCustomerType(Mockito.anyString()))
	 * .thenReturn(listOfProductUserPException);
	 * customerServiceImpl.productsList(customerId); }
	 */

	/*
	 * @Test public void productList() throws UserExistsErrorException {
	 * Mockito.when(customerRepository.findByCustomerId(Mockito.anyLong())).
	 * thenReturn(customer);
	 * Mockito.when(productRepository.findAll()).thenReturn(listOfProduct);
	 * List<ProductListResponseDto> response =
	 * customerServiceImpl.productList(customerId); Assert.assertNotNull(response);
	 * Assert.assertEquals(listOfProduct, response); }
	 */
	@Test(expected = UserExistsErrorException.class)
	public void userExistsErrorException() throws UserExistsErrorException {
		Mockito.lenient().when(customerRepository.findByCustomerId(Mockito.anyInt())).thenReturn(customer);
		customerServiceImpl.productList(customerId);
	}
	
}