package com.org.e_commerce.customerservicetest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.org.e_commerce.dto.ProductsListResponseDto;
import com.org.e_commerce.entity.Product;
import com.org.e_commerce.exception.ProductException;
import com.org.e_commerce.repository.ProductRepository;
import com.org.e_commerce.service.ProductServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceImplTest {

	@Mock
	ProductRepository productRepository;

	@InjectMocks
	ProductServiceImpl productServiceImpl;

	List<Product> listOfProducts;
	List<Product> listOfProductsEmpty;
	Product product;
	String productName;

	@Before
	public void setup() {
		listOfProducts = new ArrayList<>();
		product = new Product();
		product.setDescription("electronic");
		product.setProductId(1l);
		product.setProductName("laptop");
		product.setProductPrice(20000);
		product.setQuantity(5);
		listOfProducts.add(product);
		productName = "laptop";
		listOfProductsEmpty = new ArrayList<>();

	}

	@Test
	public void searchProduct() throws ProductException {

		Mockito.when(productRepository.findByProductNameIgnoreCaseContaining(Mockito.anyString()))
				.thenReturn(listOfProducts);
		ProductsListResponseDto actualValue = productServiceImpl.searchProduct(productName);
		Assert.assertNotNull(actualValue);
		Assert.assertEquals(700, actualValue.getStatusCode());

	}
	
	@Test(expected = ProductException.class)
	public void searchProductException() throws ProductException {

		Mockito.when(productRepository.findByProductNameIgnoreCaseContaining(Mockito.anyString()))
				.thenReturn(listOfProductsEmpty);
		productServiceImpl.searchProduct("xxx");

	}
	
	

}
