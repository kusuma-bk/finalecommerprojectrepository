package com.org.e_commerce.customerservicetest;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.org.e_commerce.dto.PurchaseDto;
import com.org.e_commerce.dto.PurchaseResponseDto;
import com.org.e_commerce.entity.ProductDetails;
import com.org.e_commerce.entity.ProductUser;
import com.org.e_commerce.entity.Purchase;
import com.org.e_commerce.exception.QuantityException;
import com.org.e_commerce.exception.QuantityUnavailable;
import com.org.e_commerce.repository.ProductDetailsRepository;
import com.org.e_commerce.repository.ProductUserRepository;
import com.org.e_commerce.repository.PurchaseRepository;
import com.org.e_commerce.service.PurchaseServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class PurchaseServiceImplTest {

	@Mock
	PurchaseRepository purchaseRepository;

	@Mock
	ProductDetailsRepository productDetailsRepository;

	@Mock
	ProductUserRepository productUserRepository;

	@InjectMocks
	PurchaseServiceImpl purchaseServiceImpl;

	PurchaseResponseDto purchaseResponseDto;
	PurchaseDto purchaseDto;
	ProductDetails productDetails;
	List<ProductUser> listOfProductUserPException;
	Purchase purchase;

	long customerId;

	@Before
	public void setup() {

		purchaseDto = new PurchaseDto();
		purchaseDto.setCustomerId(2);
		purchaseDto.setProductId(1);
		purchaseDto.setQuantity(2);

		purchaseResponseDto = new PurchaseResponseDto();
		purchaseResponseDto.setMessage("You have purchased the product successfully");
		purchaseResponseDto.setStatus(621);

		productDetails = new ProductDetails();
		productDetails.setProductDetailsId(1);
		productDetails.setProductId(2);
		productDetails.setProductRating(3);
		productDetails.setQuantity(5);

		purchase = new Purchase();
		purchase.setCustomerId(2);
		purchase.setProductId(1);
		purchase.setQuantity(3);
		purchase.setPurchasedDate(LocalDateTime.now());
	}

	@Test
	public void buyProductTest() throws QuantityException, QuantityUnavailable {
		Mockito.when(productDetailsRepository.findByProductId(Mockito.anyLong()))
				.thenReturn(Optional.of(productDetails));
		Mockito.when(purchaseRepository.save(Mockito.anyObject())).thenReturn(purchase);
		PurchaseResponseDto response = purchaseServiceImpl.buyProduct(purchaseDto);
		Assert.assertEquals(purchaseResponseDto.getMessage(), response.getMessage());
	}
}
