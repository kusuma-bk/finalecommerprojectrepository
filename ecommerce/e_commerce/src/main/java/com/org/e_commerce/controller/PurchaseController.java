package com.org.e_commerce.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.e_commerce.dto.PurchaseDto;
import com.org.e_commerce.dto.PurchaseListResponseDto;
import com.org.e_commerce.dto.PurchaseResponseDto;
import com.org.e_commerce.dto.RatingRequestDto;
import com.org.e_commerce.dto.RatingResponseDto;
import com.org.e_commerce.exception.CustomerException;
import com.org.e_commerce.exception.ProductException;
import com.org.e_commerce.exception.QuantityException;
import com.org.e_commerce.exception.QuantityUnavailable;
import com.org.e_commerce.exception.UserExistsErrorException;
import com.org.e_commerce.exception.UserPasswordErrorException;
import com.org.e_commerce.service.PurchaseService;

@RequestMapping(value = "/purchases")
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PurchaseController {
	@Autowired
	PurchaseService purchaseService;

	@PostMapping(value = "/rates")
	public ResponseEntity<RatingResponseDto> rating(@RequestBody RatingRequestDto ratingRequestDto) {
		// logger.info("Inside PurchaseController of rating method ");
		return new ResponseEntity<>(purchaseService.rating(ratingRequestDto), HttpStatus.ACCEPTED);
	}

	@PostMapping
	public ResponseEntity<PurchaseResponseDto> buyProduct(@RequestBody PurchaseDto purchaseDto)
			throws QuantityException, QuantityUnavailable {

		return new ResponseEntity<>(purchaseService.buyProduct(purchaseDto), HttpStatus.OK);
	}

	@GetMapping(value = "/{customerId}")
	public ResponseEntity<List<PurchaseListResponseDto>> purchaseList(@PathVariable long customerId) {

		// logger.info("Inside CustomerController of productsList method ");
		return new ResponseEntity<>(purchaseService.purchaseList(customerId), HttpStatus.ACCEPTED);
	}

}
