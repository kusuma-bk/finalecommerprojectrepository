package com.org.e_commerce.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.org.e_commerce.utility.CustomerUtility;
import com.org.e_commerce.utility.ProductsUtility;
import com.org.e_commerce.utility.PurchaseUtility;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
	@ExceptionHandler(CustomerException.class)
	public ResponseEntity<ErrorResponse> customerErrorException(CustomerException ex) {
		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(CustomerUtility.CUSTOMER_EXISTS_ERROR_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(ProductException.class)
	public ResponseEntity<ErrorResponse> productsErrorException(ProductException ex) {
		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(ProductsUtility.PRODUCTS_LIST_ERROR_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(UserExistsErrorException.class)
	public ResponseEntity<ErrorResponse> userExistsErrorException(UserExistsErrorException ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(CustomerUtility.USER_ERROR_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(UserPasswordErrorException.class)
	public ResponseEntity<ErrorResponse> userPasswordErrorException(UserPasswordErrorException ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(CustomerUtility.USER_PSWD_ERROR_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(UserAlreadyExists.class)
	public ResponseEntity<ErrorResponse> userAreadyExistsException(UserAlreadyExists ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(CustomerUtility.USERALREADY_EXISTS_STATUSCODE);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(QuantityException.class)
	public ResponseEntity<ErrorResponse> quantityException(QuantityException ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(PurchaseUtility.QUANTITY_ERROR_STATUSCODE);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(QuantityUnavailable.class)
	public ResponseEntity<ErrorResponse> quantityUnavailable(QuantityUnavailable ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(PurchaseUtility.QUANTITY_UNAVAILABLE_STATUSCODE);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);

	}
}
