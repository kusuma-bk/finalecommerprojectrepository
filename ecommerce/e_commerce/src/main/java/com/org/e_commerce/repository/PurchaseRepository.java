package com.org.e_commerce.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.org.e_commerce.entity.Purchase;

@Repository
public interface PurchaseRepository extends JpaRepository<Purchase, Long> {

	Optional<Purchase> findByProductIdAndCustomerIdAndTransactionId(long productId, long customerId, int transactionId);

	List<Purchase> findByCustomerId(long customerId);

}
