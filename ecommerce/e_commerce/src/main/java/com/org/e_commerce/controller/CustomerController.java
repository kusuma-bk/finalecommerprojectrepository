package com.org.e_commerce.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.e_commerce.dto.CustomerDto;
import com.org.e_commerce.dto.ProductListResponseDto;
import com.org.e_commerce.exception.UserAlreadyExists;
import com.org.e_commerce.exception.UserExistsErrorException;
import com.org.e_commerce.service.CustomerService;

@RestController
@RequestMapping("/customers")
@CrossOrigin(origins = "*", allowedHeaders = "*")

public class CustomerController {

	private static final Logger logger = LoggerFactory.getLogger(CustomerController.class);
	@Autowired
	CustomerService customerService;

	@PostMapping
	public ResponseEntity<String> customerRegistartion(@RequestBody CustomerDto customerDto) throws UserAlreadyExists {
		return new ResponseEntity<>(customerService.customerRegistartion(customerDto), HttpStatus.OK);
	}

	@GetMapping(value = "/{customerId}/products")
	public ResponseEntity<List<ProductListResponseDto>> productsList(@PathVariable long customerId)
			throws UserExistsErrorException {

		logger.info("Inside ProductController of productsList method ");
		List<ProductListResponseDto> products = customerService.productList(customerId);
		return new ResponseEntity<>(products, HttpStatus.ACCEPTED);
	}

}
