package com.org.e_commerce.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RatingRequestDto {
	private long productId;
	private int transactionId;
	private int customerRating;
	private long customerId;

}
