package com.org.e_commerce.service;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.org.e_commerce.dto.CustomerDto;
import com.org.e_commerce.dto.CustomerLoginResponseDto;
import com.org.e_commerce.dto.LoginRequestDto;
import com.org.e_commerce.dto.ProductListResponseDto;
import com.org.e_commerce.entity.Customer;
import com.org.e_commerce.entity.Product;
import com.org.e_commerce.exception.UserAlreadyExists;
import com.org.e_commerce.exception.UserExistsErrorException;
import com.org.e_commerce.exception.UserPasswordErrorException;
import com.org.e_commerce.repository.CustomerRepository;
import com.org.e_commerce.repository.ProductUserRepository;
import com.org.e_commerce.repository.ProductRepository;
import com.org.e_commerce.utility.CustomerUtility;

@Service
public class CustomerServiceImpl implements CustomerService {

	private static final Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);

	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	ProductRepository productsRepository;

	@Autowired
	ProductUserRepository productUserRepository;

	@Autowired
	ProductRepository productRepository;

	public String customerRegistartion(CustomerDto customerDto) throws UserAlreadyExists {
		Customer customer;
		logger.info("inside customerRegistartion method of CustomerServiceImpl class");

		Optional<Customer> userDetail = customerRepository.findByEmailId(customerDto.getEmailId());

		if (userDetail.isPresent()) {
			throw new UserAlreadyExists(CustomerUtility.USERALREADY_EXISTS);
		}
		customer = new Customer();
		BeanUtils.copyProperties(customerDto, customer);
		customerRepository.save(customer);
		return "CustomerDetails registered Successfully";
	}

	/*
	 * @Override public List<ProductListResponseDto> productsList(long customerId)
	 * throws CustomerException, ProductException {
	 * logger.info("inside productsList method of CustomerServiceImpl class");
	 * List<ProductUser> listOfProductUser; ProductListResponseDto
	 * productUserResponse; List<ProductListResponseDto> listproductUserResponse =
	 * new ArrayList<>();
	 * 
	 * Optional<Customer> optionalCustomer =
	 * customerRepository.findById(customerId); if (!optionalCustomer.isPresent()) {
	 * throw new CustomerException(CustomerUtility.CUSTOMER_EXISTS_ERROR); } String
	 * type = optionalCustomer.get().getCustomerType();
	 * 
	 * if ((type.equalsIgnoreCase("normal")) || (type.equalsIgnoreCase("priority")))
	 * {
	 * 
	 * listOfProductUser = productUserRepository.findByCustomerType(type); if
	 * (listOfProductUser.isEmpty()) { throw new
	 * ProductException(ProductsUtility.PRODUCTS_LIST_ERROR); }
	 * 
	 * for (ProductUser productUser : listOfProductUser) { productUserResponse = new
	 * ProductListResponseDto(); BeanUtils.copyProperties(productUser,
	 * productUserResponse); listproductUserResponse.add(productUserResponse); }
	 * 
	 * }
	 * 
	 * return listproductUserResponse;
	 * 
	 * }
	 */

	@Override
	public CustomerLoginResponseDto customerLogin(LoginRequestDto loginRequestDto)
			throws UserExistsErrorException, UserPasswordErrorException {
		logger.info("inside customerLogin method of CustomerServiceImpl class");
		Optional<Customer> userDetail = customerRepository.findByEmailIdAndPassword(loginRequestDto.getEmailId(),
				loginRequestDto.getPassword());
		if (!userDetail.isPresent()) {

			Optional<Customer> userDetail1 = customerRepository.findByEmailId(loginRequestDto.getEmailId());
			if (!userDetail1.isPresent())

				throw new UserExistsErrorException(CustomerUtility.USER_ERROR);

			throw new UserPasswordErrorException(CustomerUtility.USER_PSWD_ERROR);
		}

		CustomerLoginResponseDto customerLoginResponseDto = new CustomerLoginResponseDto();
		BeanUtils.copyProperties(userDetail.get(), customerLoginResponseDto);
		customerLoginResponseDto.setStatuscode(611);
		Random rand = new Random();

		customerLoginResponseDto.setLoginId(rand.nextInt(10000));
		return customerLoginResponseDto;
	}

	public List<ProductListResponseDto> productList(long customerId) throws UserExistsErrorException {

		Customer customer = customerRepository.findByCustomerId(customerId);
		if (ObjectUtils.isEmpty(customer)) {
			throw new UserExistsErrorException(CustomerUtility.CUSTOMER_EXISTS_ERROR);
		} else {
			String userType = customer.getCustomerType();
			if (userType.equalsIgnoreCase("priority")) {
				List<Product> products = productRepository.findAll();
				return products.stream().map(product -> {
					ProductListResponseDto productListResponseDto = new ProductListResponseDto();
					BeanUtils.copyProperties(product, productListResponseDto);
					return productListResponseDto;
				}).collect(Collectors.toList());
	
			} else {
				Long count = productRepository.count();
				PageRequest pagable = (PageRequest.of(0, (int) (count / 2),
						Sort.by(Sort.Direction.DESC, "productName")));
				Page<Product> products = productRepository.findAll(pagable);
				return products.stream().map(product -> {
					ProductListResponseDto productListResponseDto = new ProductListResponseDto();
					BeanUtils.copyProperties(product, productListResponseDto);
					return productListResponseDto;
				}).collect(Collectors.toList());
			}
		}

	}


}
