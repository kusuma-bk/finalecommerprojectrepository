package com.org.e_commerce.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.org.e_commerce.dto.ProductDto;
import com.org.e_commerce.dto.ProductListResponseDto;
import com.org.e_commerce.dto.ProductResponseDto;
import com.org.e_commerce.dto.ProductsListResponseDto;
import com.org.e_commerce.entity.Product;
import com.org.e_commerce.entity.ProductDetails;
import com.org.e_commerce.exception.ProductDetailsException;
import com.org.e_commerce.exception.ProductException;
import com.org.e_commerce.repository.ProductDetailsRepository;
import com.org.e_commerce.repository.ProductRepository;
import com.org.e_commerce.utility.ProductsUtility;
import com.org.e_commerce.utility.PurchaseUtility;

@Service
public class ProductServiceImpl implements ProductService {
	private static final Logger logger = LoggerFactory.getLogger(ProductServiceImpl.class);

	@Autowired
	ProductRepository productRepository;

	@Autowired
	ProductDetailsRepository productDetailsRepository;
	@Override
	public ProductsListResponseDto searchProduct(String productName) throws ProductException {
		List<ProductListResponseDto> listOfProductResponseDto = new ArrayList<>();
		ProductListResponseDto productListResponseDto;
		ProductsListResponseDto productsListResponseDto = new ProductsListResponseDto();
		List<Product> listOfProducts = productRepository.findByProductNameIgnoreCaseContaining(productName);
		if (listOfProducts.isEmpty()) {
			throw new ProductException(ProductsUtility.PRODUCTS_LIST_ERROR);
		}

		for (Product product : listOfProducts) {

			productListResponseDto = new ProductListResponseDto();
			BeanUtils.copyProperties(product, productListResponseDto);
			listOfProductResponseDto.add(productListResponseDto);
			productsListResponseDto.setProductListResponseDto(listOfProductResponseDto);
			productsListResponseDto.setStatusCode(ProductsUtility.PRODUCT_LIST_STATUS);
		}
		
		/*List<Product> products = productRepository.findAll();
		return products.stream().map(product -> {
			ProductListResponseDto productListResponseDto = new ProductListResponseDto();
			BeanUtils.copyProperties(product, productListResponseDto);
			return productListResponseDto;
		}).collect(Collectors.toList());
		*/
		

		return productsListResponseDto;
	}

	@Override
	public ProductResponseDto addProducts(ProductDto productDto) throws ProductDetailsException {
		logger.info("inside addProducts method of ProductServiceImpl class");
		ProductResponseDto productResponseDto = new ProductResponseDto();
		if (ObjectUtils.isEmpty(productDto)) {
			throw new ProductDetailsException(ProductsUtility.TRY_AGAIN);
		}
		Product product = new Product();
		BeanUtils.copyProperties(productDto, product);
		productResponseDto.setMessage(ProductsUtility.PRODUCT_ADDED_SUCCESS);
		productResponseDto.setStatusCode(ProductsUtility.PRODUCT_ADDED_SUCCESS_STATUS);
		productRepository.save(product);
		return productResponseDto;
	}

}
