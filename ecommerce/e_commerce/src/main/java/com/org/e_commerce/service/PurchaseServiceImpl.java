package com.org.e_commerce.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.e_commerce.dto.PurchaseDto;
import com.org.e_commerce.dto.PurchaseListResponseDto;
import com.org.e_commerce.dto.PurchaseResponseDto;
import com.org.e_commerce.dto.RatingRequestDto;
import com.org.e_commerce.dto.RatingResponseDto;
import com.org.e_commerce.entity.Product;
import com.org.e_commerce.entity.ProductDetails;
import com.org.e_commerce.entity.Purchase;
import com.org.e_commerce.exception.QuantityException;
import com.org.e_commerce.exception.QuantityUnavailable;
import com.org.e_commerce.repository.ProductDetailsRepository;
import com.org.e_commerce.repository.ProductRepository;
import com.org.e_commerce.repository.PurchaseRepository;
import com.org.e_commerce.utility.PurchaseUtility;

@Service
public class PurchaseServiceImpl implements PurchaseService {
	@Autowired
	PurchaseRepository purchaseRepository;
	@Autowired
	ProductDetailsRepository productDetailsRepository;

	@Autowired
	ProductRepository productRepository;

	@Override
	public RatingResponseDto rating(RatingRequestDto ratingRequestDto) {

		Optional<Purchase> purchase = purchaseRepository.findByProductIdAndCustomerIdAndTransactionId(
				ratingRequestDto.getProductId(), ratingRequestDto.getCustomerId(), ratingRequestDto.getTransactionId());

		if (purchase.isPresent()) {
			purchase.get().setCustomerRating(ratingRequestDto.getCustomerRating());
			purchaseRepository.save(purchase.get());

		}
		Optional<ProductDetails> product1 = productDetailsRepository.findByProductId(ratingRequestDto.getProductId());
		float rating = (product1.get().getProductRating() + ratingRequestDto.getCustomerRating()) / 2;
		product1.get().setProductRating(rating);
		productDetailsRepository.save(product1.get());
		RatingResponseDto ratingResponseDto = new RatingResponseDto();
		ratingResponseDto.setMessage("Thanks for your feedback");
		ratingResponseDto.setStatus(611);
		return ratingResponseDto;

	}

	@Override
	public PurchaseResponseDto buyProduct(PurchaseDto purchaseDto) throws QuantityException, QuantityUnavailable {

		Optional<ProductDetails> productDetails = productDetailsRepository.findByProductId(purchaseDto.getProductId());
		if (purchaseDto.getQuantity() > productDetails.get().getQuantity())
			throw new QuantityException(PurchaseUtility.QUANTITY_ERROR);
		if (productDetails.get().getQuantity() == 0)
			throw new QuantityUnavailable(PurchaseUtility.QUANTITY_UNAVAILABLE);

		Purchase purchase = new Purchase();
		purchase.setPurchasedDate(LocalDateTime.now());
		/*
		 * Random random = new Random(); purchaseDto.setTransactionId(random.nextInt(90)
		 * + 10);
		 */

		BeanUtils.copyProperties(purchaseDto, purchase);
		purchaseRepository.save(purchase);

		int quantity = (productDetails.get().getQuantity()) - (purchaseDto.getQuantity());
		System.out.println(quantity);

		productDetails.get().setQuantity(quantity);
		productDetailsRepository.save(productDetails.get());
		PurchaseResponseDto purchaseResponseDto = new PurchaseResponseDto();
		purchaseResponseDto.setMessage("You have purchased the product successfully");
		purchaseResponseDto.setStatus(621);
		return purchaseResponseDto;
	}

	@Override
	public List<PurchaseListResponseDto> purchaseList(long customerId) {

		List<Purchase> purchaseList = purchaseRepository.findByCustomerId(customerId);

		List<PurchaseListResponseDto> purchaseListResponseDto = new ArrayList<>();
		for (Purchase purchase : purchaseList) {
			PurchaseListResponseDto purchaseResponseDto = new PurchaseListResponseDto();
			LocalDateTime localDateTime = purchase.getPurchasedDate();

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			// LocalDateTime dateTime = LocalDateTime.of(1986, Month.APRIL, 8, 12, 30);
			String formattedDate = localDateTime.format(formatter);

			DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("HH:mm");
			String formattedTime = localDateTime.format(formatter1);

			BeanUtils.copyProperties(purchase, purchaseResponseDto);

			purchaseResponseDto.setTime(formattedTime);
			purchaseResponseDto.setDate(formattedDate);

			Optional<Product> product = productRepository.findByProductId(purchase.getProductId());

			purchaseResponseDto.setProductName(product.get().getProductName());
			purchaseResponseDto.setDescription(product.get().getDescription());
			purchaseResponseDto.setProductPrice(product.get().getProductPrice());

			purchaseListResponseDto.add(purchaseResponseDto);

		}

		return purchaseListResponseDto;

	}
}
