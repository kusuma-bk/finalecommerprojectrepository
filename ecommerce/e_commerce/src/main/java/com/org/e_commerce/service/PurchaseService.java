package com.org.e_commerce.service;

import java.util.List;

import com.org.e_commerce.dto.PurchaseDto;
import com.org.e_commerce.dto.PurchaseListResponseDto;
import com.org.e_commerce.dto.PurchaseResponseDto;
import com.org.e_commerce.dto.RatingRequestDto;
import com.org.e_commerce.dto.RatingResponseDto;
import com.org.e_commerce.exception.QuantityException;
import com.org.e_commerce.exception.QuantityUnavailable;

public interface PurchaseService {
	RatingResponseDto rating(RatingRequestDto ratingRequestDto);

	public PurchaseResponseDto buyProduct(PurchaseDto purchaseDto) throws QuantityException, QuantityUnavailable;

	List<PurchaseListResponseDto> purchaseList(long customerId);
}
