package com.org.e_commerce.exception;

public class QuantityUnavailable extends Exception {
	private static final long serialVersionUID = 1L;

	public QuantityUnavailable(String message) {
		super(message);
	}

}
