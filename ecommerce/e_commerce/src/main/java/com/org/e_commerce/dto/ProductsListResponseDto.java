package com.org.e_commerce.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProductsListResponseDto {
	List<ProductListResponseDto> productListResponseDto;
	private int statusCode;
}
