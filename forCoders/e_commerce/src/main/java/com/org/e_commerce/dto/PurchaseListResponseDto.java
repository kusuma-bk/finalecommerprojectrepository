package com.org.e_commerce.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PurchaseListResponseDto {
	private long purchaseId;
	private int quantity;
	private int customerRating;
	private String productName;
	private double productPrice;
	private String description;
	private String time;
	private String date;

}
