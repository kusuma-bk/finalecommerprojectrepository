package com.org.e_commerce.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProductListResponseDto {
	private long productId;
	private String productName;
	private String description;
	private double productPrice;
	private int quantity;
	private int productRating;

	@Override
	public String toString() {
		return "ProductListResponseDto [productId=" + productId + ", productName=" + productName + ", description="
				+ description + ", productPrice=" + productPrice + ", quantity=" + quantity + ", productRating="
				+ productRating + "]";
	}

}
