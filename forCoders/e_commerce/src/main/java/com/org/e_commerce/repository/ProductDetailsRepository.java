package com.org.e_commerce.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.org.e_commerce.entity.ProductDetails;

@Repository
public interface ProductDetailsRepository extends JpaRepository<ProductDetails, Long> {

	Optional<ProductDetails> findByProductId(long productId);

	Optional<ProductDetails> findByProductIdOrderByQuantityDesc(long productId);

}
