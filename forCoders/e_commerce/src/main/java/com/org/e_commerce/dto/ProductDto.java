package com.org.e_commerce.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProductDto {
	private String productName;

	private double productPrice;

	private String description;

	private int quantity;

}
