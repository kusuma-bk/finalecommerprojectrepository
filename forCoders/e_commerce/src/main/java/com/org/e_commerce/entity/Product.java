package com.org.e_commerce.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)

	private long productId;
	private String productName;
	private double productPrice;
	private String description;
	private int quantity;

	@Override
	public String toString() {
		return "Product [productId=" + productId + ", productName=" + productName + ", productPrice=" + productPrice
				+ ", description=" + description + ", quantity=" + quantity + ", getProductId()=" + getProductId()
				+ ", getProductName()=" + getProductName() + ", getProductPrice()=" + getProductPrice()
				+ ", getDescription()=" + getDescription() + ", getQuantity()=" + getQuantity() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}

}
